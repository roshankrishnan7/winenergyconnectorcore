# ORDER API

* API to process payments for WinEnergy

### What is this repository for?

* API to process transactions for WinEnergy

### Version 
* 1.0

### Stack

* `.net core 2.1`

### How do I get set up?

* **Publish**
    ``` dotnet publish WinEnergyConnectorCore.csproj ```

* **Deployment instructions**
    * Follow instructions for hosting .net core applications.  https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/