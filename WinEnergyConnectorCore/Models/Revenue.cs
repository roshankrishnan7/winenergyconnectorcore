﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Connector.Models
{
    public class Revenue
    {
        public DateTime date { get; set; }
        public string account { get; set; }
        public double amount { get; set; }
        public string description { get; set; }
        public string distCode { get; set; }
    }
}