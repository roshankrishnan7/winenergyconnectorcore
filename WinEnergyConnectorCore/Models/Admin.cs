﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using Microsoft.AspNetCore.Http;

namespace Connector.Models
{
    public class Admin
    {
        public static string username;
        public static string password;
        public static string server;

        public Admin(bool test)
        {
            String fileName = HttpContext.Current.ApplicationInstance.Server.MapPath("~/App_Data/sage.xml");
            if (File.Exists(fileName))
            {
                XDocument doc = XDocument.Load(fileName);
                string mode = "test";
                if (!test)
                    mode = "live";
                foreach (XElement el in doc.Root.Elements())
                {
                    var s = doc.Root.Document.ToString();
                    if (el.Name == "Application" && el.Attribute("app").Value == mode)
                    {
                        foreach (XElement element in el.Elements())
                        {
                            if (element.Name == "user")
                                username = element.Value;
                            if (element.Name == "pass")
                                password = element.Value;
                            if (element.Name == "db")
                                server = element.Value;
                        }

                    }
                }
            }
        }
    }
}