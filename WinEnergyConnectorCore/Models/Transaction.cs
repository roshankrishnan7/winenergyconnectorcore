﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Connector.Models
{
    public class Transaction
    {
        public DateTime date { get; set; }
        public string type { get; set; }
        public string payment { get; set; }
        public double amount { get; set; }
        public string description { get; set; }
        public string reference { get; set; }
    }
}