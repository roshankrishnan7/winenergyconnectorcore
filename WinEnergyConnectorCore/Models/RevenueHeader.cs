﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Connector.Models
{
    public class RevenueHeader
    {
        public DateTime date { get; set; }
        public string description { get; set; }
        public List<Revenue> revenueList { get; set; }
    }
}